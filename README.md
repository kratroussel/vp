# Индикатор VP

Распределение сделок по ценовым уровням на заданном временном участке.

![Индикатор VP в двух режимах](media/mpvl_daily_rm2.png)

## Параметры

- **Mode** / Mode: режим работы:
	- `Period Mode`: разбивка по периодам
	- `Range Mode`: один диапазон
- **PERIOD MODE**: параметры для Period Mode
	- **Range Period** / RangePeriod: период, только стандартные значения от `M1` до `MN1`
	- **Range Count** / RangeCount: количество диапазонов
	- **Time Zone Shift** / TimeShift: сдвиг часового пояса от -12 до +12 часов с шагом 1 час
	- **Histogram Position** / HgPosPeriod: расположение/направление гистограммы:
		- `Left to Right`: слева направо
		- `Right to Left`: справа налево
		- `Left to Center`: от левого края до центра
		- `Center to Left`:  от центра до левого края
		- `Center to Right`: от центра до правого края
		- `Right to Center`: от правого края до центра
	- **Zoom Type** / ZoomType: тип масштаба:
		- `Custom Zoom...`: пользовательский, один на все гистограммы
		- `Automatic Zoom (global)`: автоматический, один на все
		- `Automatic Zoom (local)`: автоматический, раздельно для каждой гистограммы
	- **Custom Zoom** / ZoomCustom: значение пользовательского масштаба
- **RANGE MODE**: параметры для Range Mode
	- **Range Selection** / RangeSelection: способ указания границ диапазона:
		- `Between lines`: между двумя линиями
		- `Last minutes`: последние **Range Size** минут
		- `Minutes to line`: **Range Size** минут до правой линии
		- `Last bars`: последние **Range Size** баров
		- `Bars to line`: **Range Size** баров до правой линии
	- **Range Size (minutes or bars)** / RangeSize: количество минут или баров для **Range Selection** (кроме `Between Lines`)
	- **Histogram Position** / HgPosition: расположение гистограммы:
		- `Window left`: левый край окна
		- `Window right`: правый край окна
		- `Left outside`: от левой границы диапазона влево
		- `Right outside`: от правой границы диапазона вправо
		- `Left inside`: от левой границы диапазона вправо
		- `Right inside`: от правой границы диапазона влево
- **DATA** / Параметры источника данных
	- **Data Source** / DataSource: Источник данных:
		- `Ticks` (только в MT5): тики с сервера вашего брокера (если есть)
		- `M1`..`D1`: интерполированные тики на основе баров указанного таймфрейма (см. **Bar Distribution**)
	- **Bar Distribution** / BarTicks: распределение тиков внутри бара
	- **Volume Type** (MT5) / VolumeType (только в MT5): тип объема, тиковый или реальный, реальный объем у брокера может быть недоступен, в этом случае индикатор ничего не покажет
- **TICK** (MT5): параметры тиков (только в MT5)
	- **Price Type** / TickPriceType: тип цены:
		- `Bid Price`: цена Bid
		- `Ask Price`: цена Ask
		- `Last Price`: цена Last
		- `Bid/Ask Average`: среднее Bid и Ask
		- `Last or Bid/Ask Average`: Last, либо среднее цен Bid и Ask, если цена Last не указана
	- **Bid Price Changed** / TickBid: тик изменил цену бид
	- **Ask Price Changed** / TickAsk: тик изменил цену аск
	- **Last Price Changed** / TickLast: тик изменил цену последней сделки
	- **Volume Changed** / TickVolume: тик изменил объем
	- **Buy Deal** / TickBuy: тик возник в результате сделки на покупку
	- **Sell Deal** / TickSell: тик возник в результате сделки на продажу
- **CALCULATION**: Параметры вычислений
	- **Mode Step (points)** / ModeStep: минимальный шаг между модами, подбирается субъективно для каждого инструмента
	- **Point Scale** / HgPointScale: масштаб пункта при рисовании гистограммы, большее значение означает более быструю работу, но более грубое отображение
	- **Smooth Depth** / Smooth: степень сглаживания, `0` для отключения
- **HISTOGRAM**: гистограмма
	- **Bar Style** / HgBarStyle: Стиль баров гистограммы:
		- `Lines`: линии
		- `Empty bar`: пустые прямоугольники
		- `Filled bar`: заполненные прямоугольники
		- `Outline`: контур
		- `Color`: цвет
	- **Coloring** / HgColoring: Способ раскраски гистограммы:
		- `No histogram`: не показывать гистограмму
		- `1st color only`: только первый цвет
		- `2nd color only`: только второй цвет
		- `Split by mean`: разделить по среднему (цвета 1 и 2)
		- `Split by Q1 (25%)`: разделить по первому квартилю (цвета 1 и 2)
		- `Split by Q2 (50%, median)`: разделить по второму квартилю - медиане (цвета 1 и 2)
		- `Split by Q3 (75%)`: разделить по третьему квартилю (цвета 1 и 2)
		- `Quartile gradient`: квартильный градиент (4 цвета от 1 до 2)
		- `Gradient (10 levels)`: 10-уровневый градиент (10 цветов от 1 до 2)
		- `Gradient (50 levels)`: 50-уровневый градиент (50 цветов от 1 до 2)
	- **Color 1** / HgColor: цвет гистограммы 1, `None` для использования цвета фона графика
	- **Color 2** / HgColor2: цвет гистограммы 2, `None` для использования цвета фона графика
	- **Line Width** / HgLineWidth: толщина линии при рисовании гистограммы
	- **Histogram Width (% of normal)** / HgWidthPct: Ширина гистограммы (% от нормальной), при отображении внутри диапазона нормальная ширина равна 100%, в остальных случаях - 15%
- **LEVELS**: уровни
	- **Mode Color** / ModeColor: цвет локальных максимумов (мод распределения), `None` для отключения
	- **Maximum Color** / MaxColor: цвет максимума, `None` для отключения
	- **Mode Line Width** / ModeLineWidth: толщина линий мод
	- **VWAP Color** / VwapColor: цвет VWAP (средневзвешенной по объёму цены), `None` для отключения
	- **Quantiles** / Quantiles: квантили:
		- `No quantiles or median`: без квантилей и медианы
		- `Median`: медиана (Q2)
		- `Quartiles`: квартили (Q1, Q2, Q3)
		- `Deciles`: децили
		- `70%: 15% - 50%(Median) - 85%`: 70% диапазон между крайними квантилями
		- `95%: 2.5% - 50%(Median) - 97.5%`: 95% диапазон между крайними квантилями
		- `99%: 0.5% - 50%(Median) - 99.5%`: 99% диапазон между крайними квантилями
	- **Quantile Color** / QuantileColor: цвет линий квантилей
	- **Quantile & VWAP Line Width** / StatLineWidth: толщина линий квантилей и VWAP
	- **Quantile & VWAP Line Style** / StatLineStyle: стиль линий квантилей и VWAP
- **LEVEL LINES (range mode only)**: линии уровней (только для Range Mode)
	- **Mode Level Line Color (None=disable)** / ModeLevelColor: цвет уровней, проведенных по модам, `None` для отключения
	- **Mode Level Line Width** / ModeLevelWidth: толщина линий
	- **Mode Level Line Style** / ModeLevelStyle: стиль линий уровней мод
- **SERVICE**: служебные
	- **Show Data Horizon** / ShowHorizon: показывать горизонт данных
	- **Identifier** / Id: идентификатор индикатора, используйте разные значения для нескольких копий индикатора на одном графике

## См. также

В блоге: <https://www.fxcoder.ru/search/label/%7BVP%7D>

Установка: <https://www.fxcoder.ru/p/install-script.html>

:flag_gb:

Install: <https://www.fxcoder.ru/p/install-script-en.html>
