/*
Copyright 2021 FXcoder

This file is part of VPSimple.

VPSimple is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VPSimple is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VPSimple. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "VPSimple 3.0. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict

#include "VPSimple-include/bsl.mqh"
#include "VPSimple-include/util/stat.mqh"
#include "VPSimple-include/volume/vp_calc.mqh"


int OnInit()
{
	// show EA-Line indicators in visual testing, see https://www.fxcoder.ru/2013/07/ea-line-indicator.html
	if (MQLInfoInteger(MQL_VISUAL_MODE))
	{
		iCustom(_Symbol, PERIOD_CURRENT, "FXcoder/EA-Line/EA-Line0", "vp-max-price");
		iCustom(_Symbol, PERIOD_CURRENT, "FXcoder/EA-Line/EA-Line", "vp-max-volume");
		iCustom(_Symbol, PERIOD_CURRENT, "FXcoder/EA-Line/EA-Line", "vp-total-volume");
	}

	return(INIT_SUCCEEDED);
}

void OnTick()
{
	// time range for 1000 last bars

	datetime time_to = TimeCurrent();
	datetime time_arr[];

	if (CopyTime(_Symbol, PERIOD_M1, time_to, 1000, time_arr) < 1)
		return;

	datetime time_from = time_arr[0];


    // histogram

	int hg_point_scale = 10;
	double hg_point = _Point * hg_point_scale;
	CVPDataParams data_params(VP_SOURCE_M1, VP_BAR_TICKS_OHLC, VOLUME_TICK);
	CVPCalc vpcalc_(data_params, hg_point, VP_TICK_PRICE_LAST, 0, QUANTILE_NONE);

	double low_price;
	double volumes[];
	const int count = vpcalc_.get_hg(time_from, time_to, low_price, volumes);
	//const int count = vpcalc_.get_hg_by_ticks(time_from, time_to, low_price, volumes);

	if (count <= 0)
		return;


	// levels

	int mode_step = 100 / hg_point_scale;
	int modes[];

	int mode_count = hg_modes(volumes, mode_step, modes);
	int max_pos = _arr.max_index(volumes);
	int median_pos = _math.median_index(volumes);
	int vwap_pos = hg_vwap_index(volumes, low_price, hg_point);


	// max

	double max_volume_price = low_price + hg_point * max_pos;
	double max_volume = volumes[max_pos];


	// modes

	for (int i = 0; i < mode_count; i++)
	{
		double mode_price = low_price + hg_point * modes[i];
		double mode_volume = volumes[modes[i]];

		// do something with the mode...
	}

	// send data to EA-Line
	if (MQLInfoInteger(MQL_VISUAL_MODE))
	{
		GlobalVariableSet("vp-max-price", max_volume_price);
		GlobalVariableSet("vp-max-volume", max_volume);
		GlobalVariableSet("vp-total-volume", _math.sum(volumes));
	}
}

/*
Последние изменения:

3.0:

	* версия на основе кода VP-v10

2.0:

	* версия на основе кода VP-v9

1.0:

	* версия на основе кода VP-v6
*/
